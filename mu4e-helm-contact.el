;;; mu4e-helm-contact.el --- Contact insertion with helm for mu4e  -*- lexical-binding: t; -*-

;; Copyright (C) 2017-2018  Anders Johansson
;; Author: Anders Johansson <mejlaandersj@gmail.com>
;; Keywords: mail, convenience
;; URL: http://www.gitlab.com/andersjohansson/mu4e-helm-contact
;; Modified: 2021-03-15
;; Package-Requires: ((emacs "25.1") (mu4e "1.2") (helm))

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Bind ‘mu4e-helm-contact-insert’ to a key you find suitable.
;;
;; Inspiration
;; http://kitchingroup.cheme.cmu.edu/blog/2015/03/14/A-helm-mu4e-contact-selector/
;; http://pragmaticemacs.com/emacs/tweaking-email-contact-completion-in-mu4e/
;; http://pragmaticemacs.com/emacs/even-better-email-contact-completion-in-mu4e/

;;; Code:
(eval-when-compile
  (require 'cl-macs))
(require 'mu4e)
(require 'helm)
(require 'helm-mode)
(require 'subr-x)

(defvar mu4e-helm-contact-map
  (let ((map (copy-keymap helm-comp-read-map)))
    (define-key map "," #'mu4e-helm-contact-persistent-insert)
    map))

(defcustom mu4e-helm-contact-alias-file nil
  "Contact aliases file. Will be added first to completion list.

Good for making groups etc. Alias and real expansion is added on
one line separated with tab. Set to nil to disable"
  :group 'mu4e
  :type '(choice (const :tag "Disable" nil)
                 (file :must-match t)))

(defvar mu4e-helm-contact--notfetched t "Used for checking fetch status.")

(defun mu4e-helm-contact-insert-action (_c)
  (with-helm-current-buffer
    (insert (mapconcat #'identity (helm-marked-candidates) ", "))))

(defun mu4e-helm-contact-persistent-insert (_c)
  (interactive)
  (with-helm-current-buffer
    (insert (mapconcat #'identity (helm-marked-candidates) ", ") ", "))
  (helm-unmark-all)
  (helm-set-pattern ""))

(defun mu4e-helm-contact-insert ()
  "Insert mu4e contacts with the help of helm."
  (interactive)
  (unless (hash-table-p mu4e~contacts-hash)
    (mu4e-helm-contact-request-contacts-wait))
  (cl-letf
      (((symbol-function 'helm-current-position) #'ignore)
       (spacecomma "[ ,]+"))
    (helm :sources (helm-build-sync-source "Insert contact"
                     :candidates (delq nil
                                       (delete-dups
                                        (append
                                         (mu4e-helm-contact--get-alias-list)
                                         (hash-table-keys mu4e~contacts-hash))))
                     :action #'mu4e-helm-contact-insert-action
                     :persistent-action #'mu4e-helm-contact-persistent-insert
                     ;; :fuzzy-match t
                     :keymap mu4e-helm-contact-map))
    (when (search-backward-regexp spacecomma (- (point) 6) t)
      (replace-match ""))))

(defun mu4e-helm-contact--get-alias-list ()
  "Return the list of email aliases."
  (when mu4e-helm-contact-alias-file
    (with-temp-buffer
      (insert-file-contents mu4e-helm-contact-alias-file) 
      (cl-loop for line in (split-string (buffer-string) "\n" t)
               collect (let ((ll (split-string line "	" t " ")))
                         (cons (car ll) (cadr ll)))))))

(defun mu4e-helm-contact--signal-fetch (&rest _ignore)
  (setq mu4e-helm-contact--notfetched nil))

(defun mu4e-helm-contact-request-contacts-wait ()
  (unwind-protect
      (let ((mu4e-compose-complete-addresses t))
        (advice-add 'mu4e~update-contacts :after #'mu4e-helm-contact--signal-fetch)
        (mu4e~request-contacts-maybe)
        (while mu4e-helm-contact--notfetched (sleep-for 0.1)))
    (advice-remove 'mu4e~update-contacts #'mu4e-helm-contact--signal-fetch)
    (setq mu4e-helm-contact--notfetched t)))

;;; Hook
(defun mu4e-helm-contact-hook-insert (&rest _ignore)
  "Select recipients if we are in an empty To-field."
  (when (and (eq major-mode 'mu4e-compose-mode)
             (message-point-in-header-p)
             (save-excursion (beginning-of-line)
                             (looking-at-p "^To:[[:space:]]+$")))
    (mu4e-helm-contact-insert)))

(defvar mu4e-helm-contact-insert-functions
  '(mu4e~compose-mail
    mu4e-compose-new
    mu4e-compose-forward)
  "Invocations to automatically launch `mu4e-helm-contact-insert' after.")

(cl-loop for x in mu4e-helm-contact-insert-functions
         do (advice-add x :after #'mu4e-helm-contact-hook-insert))

(provide 'mu4e-helm-contact)
;;; mu4e-helm-contact.el ends here
